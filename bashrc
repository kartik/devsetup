# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
[ -z "$PS1" ] && return

# History hacks
# Don't put duplicate lines in the history.
export HISTCONTROL=ignoredups
# Ignores faltoo commands
export HISTIGNORE="&:ls:[bf]g:exit"
export HISTFILESIZE=
export HISTSIZE=
export HISTTIMEFORMAT='[%F %T] '

# records all session's history
# See: http://www.bradshawenterprises.com/blog/2008/09/27/5-ways-to-make-using-bash-more-productive/
shopt -s histappend
PROMPT_COMMAND='history -a'

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "$debian_chroot" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

#Josh's prompt from: http://maketecheasier.com/8-useful-and-interesting-bash-prompts/2009/09/04
PS1="\[\033[35m\]\t\[\033[m\]-\[\033[36m\]\u\[\033[m\]@\[\033[32m\]\h:\[\033[33;1m\]\w\[\033[m\]\$ "

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PROMPT_COMMAND='echo -ne "\033]0;${USER}@${HOSTNAME}: ${PWD/$HOME/~}\007"'
    ;;
*)
    ;;
esac

#enable color support of ls and also add handy aliases
if [ "$TERM" != "dumb" ]; then
    eval "`dircolors -b`"
    alias ls='ls --color=auto'
    alias dir='ls --color=auto --format=vertical'
    alias vdir='ls --color=auto --format=long'
fi

#For colourful man pages (CLUG-Wiki style)
export LESS_TERMCAP_mb=$'\E[01;31m'
export LESS_TERMCAP_md=$'\E[01;31m'
export LESS_TERMCAP_me=$'\E[0m'
export LESS_TERMCAP_se=$'\E[0m'
export LESS_TERMCAP_so=$'\E[01;44;33m'
export LESS_TERMCAP_ue=$'\E[0m'
export LESS_TERMCAP_us=$'\E[01;32m'

#PATH
export PATH=$PATH:$HOME/.bin:$HOME/.local/bin:/usr/local/bin

#some more ls aliases
alias la='ls -A'
alias l='ls -CF'

#for easy navigation in shell!
alias cd..='cd ..'
alias .='pwd'
alias ..='cd ..'
alias ...='cd ../..'
alias cl='clear'
alias grep='grep --color=auto --exclude-dir={.bzr,CVS,.git,.hg,.svn}'
alias bye='clear; exit'

#more alias for makes thing easier
alias diff='colordiff'
alias tail='colortail'
alias ip='sudo ifconfig -a | grep "inet " | tail -1 | cut -c14-24'
alias aria2c='aria2c --file-allocation=none'
alias pingg='ping -c 4 www.google.com'
alias bc='bc -l'
alias pdf='zathura'
alias cat='batcat'
alias lsblkt='lsblk -e 7 -o name,size,type,fstype,mountpoint'
alias dut='du -sh'
alias dft='df -hT -x squashfs -x tmpfs -x devtmpfs'

#make sure we don't do horrible things..
alias rm='rm -i'

#Debian stuffs
export DEBEMAIL=kartik@debian.org
export DEBFULLNAME="Kartik Mistry"
export EDITOR='vim'
export VISUAL='vim'
alias update='sudo apt update'
alias upgrade='sudo apt upgrade'
alias auc='sudo apt autopurge && sudo apt autoclean'
alias lintian='lintian -EviI --pedantic --color auto --tag-display-limit 0'
alias lintian-normal='lintian --color auto --tag-display-limit 0'
alias debs-by-size='grep-status -FStatus -sInstalled-Size,Package -n "install ok installed" | paste -sd "  \n" | sort -rn'
alias psign='debsign -k6B631B5EF554AF6B1196629202C1D3F2783AA4DE *.changes'
alias blhc='blhc --debian --line-numbers --color'

#apt-cache function from LG
ac () { /usr/bin/apt-cache search "$@"|LESS=FX /usr/bin/less; }
export -f ac

cdf(){
 local local_path="$(pwd | rev | cut -d/ -f2- | rev | grep -o -E ".*$1[^/]*")"
 echo cd -- "$local_path"
 cd -- "$local_path"
}

#MW specific
export MW=/home/kartik/Development/WP/mediawiki-core

core_clone() {
    git clone ssh://kartik@gerrit.wikimedia.org:29418/mediawiki/core.git $1;
}

ext_clone() {
    git clone ssh://kartik@gerrit.wikimedia.org:29418/mediawiki/extensions/$1;
 }

localset() { vim $MW/LocalSettings.php; }

mwupdate() { php $MW/maintenance/update.php --quick; }

alias core='cd $MW'
alias cx='cd ~/Development/WP/ContentTranslation'
alias cxs='cd ~/Development/WP/cxserver'
alias twn='cd ~/Development/WP/translatewiki'
alias tx='cd ~/Development/WP/Translate'
alias ux='cd ~/Development/WP/UniversalLanguageSelector'

#Simple HTTP server
pshs() { python -m SimpleHTTPServer; }
export -f pshs

#Everybody should have fun!
alias entertain='mplayer "$(find "/home/kartik/Media" -type f -regextype posix-egrep -regex ".*\.(avi|mkv|flv|mpg|mpeg|mp4|wmv|3gp|mov|divx)" | shuf -n1)"'
alias rndm='tr -c "[:digit:]" " " < /dev/urandom | dd cbs=$COLUMNS conv=unblock | GREP_COLOR="1;32" grep --color "[^ ]"'
alias stardate='date "+%y%m.%d/%H%M"' # from Joey
alias કાર્તિક='toilet -f ivrit 'Kartik' | boxes -d cat -a hc -p h8 | lolcat -a'
alias sleepycat='echo ᓚᘏᗢ'

#git
alias master='git checkout master'
alias prod='git checkout production'
alias review='git fetch gerrit; git review;'
alias gst='git show HEAD'
alias gd='git diff'
alias gfa='git fetch --all'
alias gstats='git shortlog --summary --numbered --all --no-merges'

#Fix for git sign!
export GPG_TTY=$(tty)

#Rust
source "$HOME/.cargo/env"

#LESS is more
export LESS="--quit-if-one-screen"

#nvm foo
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
