#!/bin/sh

DATE=$(date +"%Y%m%d")
DOT="/home/kartik/Development/Debian/devsetup"

cp ~/.bashrc    $DOT/bashrc
cp ~/.gbp.conf  $DOT/gbp.conf
cp ~/.gitconfig $DOT/gitconfig
cp ~/.mrconfig  $DOT/mrconfig
cp ~/.quiltrc   $DOT/quiltrc
cp ~/.tmux.conf $DOT/tmux.conf
cp ~/.vimrc     $DOT/vimrc
cp ~/.mrconfig  $DOT/mrconfig
cp ~/Development/WP/.mrconfig $DOT/mrconfig.wp

cp -r /etc/apt/sources.list.d/*.sources $DOT/sources.list.d

dpkg --get-selections > $DOT/packages.txt && echo "Package list backed up!"

cd "$DOT" || exit

if git diff --quiet; then
    echo "No changes to commit."
else
    git add -A
    git commit -m "Updated $(git diff --name-only) on $DATE"
    git push
fi
